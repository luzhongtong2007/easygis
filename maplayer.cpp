#include <QObject>

#include <maplayer.h>
#include <mapcanvas.h>
#include <QDateTime>

namespace EasyGIS {

MapLayer::MapLayer(
    const QString &id,
    EasyGIS::CRS *crs,
    EasyGIS::MapCanvas *mapCanvas)
    : mId(id),
      mCrs(crs),
      mZValue(-1),
      mZoomValue(kDefaultZoomValue),
      mProvider(nullptr),
      mMapCanvasMap(new MapCanvasMap(this)),
      mMapCanvas(mapCanvas) {

}

MapLayer::~MapLayer() {
  delete mCrs;
  delete mProvider;
}

MapLayer::MapLayer(const MapLayer &other) {
  mId = other.mId;
  mZValue = other.mZValue;
  mCrs = other.mCrs;
  mMapCanvas = other.mMapCanvas;
}

MapLayer::MapLayer(MapLayer &&other) noexcept {
  mId = other.mId;
  mZValue = other.mZValue;
  mCrs = other.mCrs;
  mMapCanvas = other.mMapCanvas;

  other.mId = QString{};
  other.mCrs = nullptr;
  other.mZValue = 0;
  other.mMapCanvas = nullptr;
}

MapLayer &
MapLayer::operator=(const MapLayer &other) {
  if (this != &other) {
    mId = other.mId;
    mZValue = other.mZValue;
    mCrs = other.mCrs;
    mMapCanvas = other.mMapCanvas;
  }

  return *this;
}

MapLayer &
MapLayer::operator=(MapLayer &&other) noexcept {
  if (this != &other) {
    mId = other.mId;
    mZValue = other.mZValue;
    mCrs = other.mCrs;
    mMapCanvas = other.mMapCanvas;

    other.mId = QString{};
    other.mCrs = nullptr;
    other.mZValue = 0;
    other.mMapCanvas = nullptr;
  }

  return *this;
}

void
MapLayer::update() {
  if(!isVisible()){
    qDebug() << id() << "=>图层不显示，跳过刷新操作";
    mMapCanvasMap->hide();
    return;
  }
  //qDebug() << "刷新图层内容=>" << mMapCanvas->viewExtent()<<QDateTime::currentDateTime()<<"zoom"<<mZoomValue;
  mProvider->createTask(mMapCanvas->viewExtent(), mZoomValue);
  mMapCanvasMap->show();
}

}

