#include "maparea.h"
#include "maplayer.h"
#include <QPainter>
#include <QDateTime>
void EasyGIS::Maparea::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{


    painter->setPen(this->pen());
    painter->setBrush(this->brush());
    double r=mLayer->resolution();
    QPolygonF scene_points;
    foreach(QPointF point,this->mkt_points){
        scene_points.append(point/r);
    }
    painter->drawPolygon(scene_points);
}

QRectF EasyGIS::Maparea::boundingRect() const
{
    if(this->mkt_points.size()==0){
        return QRectF();
    }
    double r=mLayer->resolution();
    QRectF rect= this->mkt_points.boundingRect();
    return QRectF(rect.x()/r,rect.y()/r,rect.width()/r,rect.height()/r);
}

void EasyGIS::Maparea::append(QPointF mkt_point)
{
    this->mkt_points.append(mkt_point);
}

QPointF &EasyGIS::Maparea::lastMktpoint()
{
    return this->mkt_points.last();
}
