#include <QtCore/QString>
#include <provider/tms/ostnormalprovider.h>

namespace EasyGIS {

OSTNormalProvider::OSTNormalProvider(QObject *parent)
    : TmsProvider(parent) {
  initCache();
}

QString
OSTNormalProvider::tileUrl(const EasyGIS::PointXY &pos, int zoom) const {
  QString urlFmt = {"https://%1.tile.openstreetmap.org/%2/%3/%4.png"};
  return QString(urlFmt).arg(server()).arg(zoom).arg(pos.x()).arg(pos.y());
}

}

