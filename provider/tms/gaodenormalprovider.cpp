#include <QtCore/QString>
#include <provider/tms/gaodenormalprovider.h>

namespace EasyGIS {

GaodeNormalProvider::GaodeNormalProvider(QObject *parent)
    : TmsProvider(parent) {
  initCache();
}

QString
GaodeNormalProvider::tileUrl(const EasyGIS::PointXY &pos, int zoom) const {
  QString urlFmt = {R"(https://webst%1.is.autonavi.com/appmaptile?style=7&x=%2&y=%3&z=%4)"};
  return QString(urlFmt).arg(server()).arg(pos.x()).arg(pos.y()).arg(zoom);
}

}



