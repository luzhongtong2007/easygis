#include <crs.h>
#include <crs/webmercator.h>

namespace EasyGIS
{
  PointXY
  WebMercator::forward(const EasyGIS::PointXY& point) const
  {
    PointXY result{};
    PointXY originXy = CRS::forward(point);

    QSizeF sz = extent().size();
    result.setX(sz.width() / 2 + originXy.x());
    result.setY(sz.height() / 2 - originXy.y());

    return result;
  }

  PointXY
  WebMercator::inverse(const EasyGIS::PointXY& point) const
  {
    PointXY originXy{};
    QSizeF sz = extent().size();

    originXy.setX(point.x() - sz.width() / 2);
    originXy.setY(sz.height() / 2 - point.y());

    return CRS::inverse(originXy);
  }

  QString
  WebMercator::proj4Cvt() const
  {
    return QString{"+proj=webmerc +datum=WGS84"};
  }

  QString
  WebMercator::wktDef() const
  {
    return QString{
      R"(
    PROJCS["WGS 84 / Pseudo-Mercator",
      GEOGCS["WGS 84",
        DATUM["WGS_1984",
          SPHEROID["WGS 84",6378137,298.257223563,
            AUTHORITY["EPSG","7030"]],
            AUTHORITY["EPSG","6326"]],
            PRIMEM["Greenwich",0,
              AUTHORITY["EPSG","8901"]],
              UNIT["degree",0.0174532925199433,
                AUTHORITY["EPSG","9122"]],
                AUTHORITY["EPSG","4326"]],
                PROJECTION["Mercator_1SP"],
                PARAMETER["central_meridian",0],
                PARAMETER["scale_factor",1],
                PARAMETER["false_easting",0],
                PARAMETER["false_northing",0],
                UNIT["metre",1,
                  AUTHORITY["EPSG","9001"]],
                  AXIS["X",EAST],
                  AXIS["Y",NORTH],
                  EXTENSION["PROJ4",
                    "+proj=merc
                      +a=6378137
                      +b=6378137
                      +lat_ts=0.0
                      +lon_0=0.0
                      +x_0=0.0
                      +y_0=0
                      +k=1.0
                      +units=m
                      +nadgrids=@null
                      +wktext
                      +no_defs"],
                    AUTHORITY["EPSG","3857"]
      ])"
    };
  }

  QRectF
  WebMercator::extent() const
  {
    return QRectF{0, 0, 40075016.686, 40075016.686};
  }

  QString
  WebMercator::proj4Def() const
  {
    return QString{
      R"(
    +proj=merc
    +a=6378137
    +b=6378137
    +lat_ts=0.0
    +lon_0=0.0
    +x_0=0.0
    +y_0=0
    +k=1.0
    +units=m
    +nadgrids=@null
    +wktext
    +no_defs)"
    };
  }
}
