#pragma once

#include <QHash>

#include <layerprovider.h>

namespace EasyGIS {
/**
 * 支持的图层Provider类型
 */
enum TmsProviders {
  OSTNormalMap,
  GaodeNormapMap
};

/**
 * TMS图层创建工厂
 */
class TmsProviderFactory {
 public:
  TmsProviderFactory() = delete;
  ~TmsProviderFactory() = default;

  static LayerProvider *
  create(TmsProviders provider);

 protected:
  static QHash<TmsProviders, LayerProvider *> mProviders;

};

}
