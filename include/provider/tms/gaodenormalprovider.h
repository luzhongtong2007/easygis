#pragma once

#include <QtCore/QSize>
#include <QtCore/QString>

#include <pointxy.h>
#include <provider/tmsprovider.h>
#include <utils/easygis.h>

namespace EasyGIS {

/**
 * 高德地图基础地图
 */
class GaodeNormalProvider : public TmsProvider {
  Q_OBJECT

 public:
  explicit GaodeNormalProvider(QObject *parent = nullptr);
  ~GaodeNormalProvider() override = default;

 public:
  /**
   * 获取瓦片的大小
   * @return 瓦片大小，一般为256*256
   */
  const QSize tileSize() const override {
    return QSize{256, 256};
  }

  /**
   * 获取高德地图的瓦片服务器编号
   * @return
   */
  QString server() const { return QString("0%1").arg(randomInt(1, 4)); }

  /**
   * 获取瓦片url路径
   * @param pos 瓦片位置
   * @param zoom zoom值
   * @return 瓦片url，失败时为空字符串
   */
  QString tileUrl(const PointXY &pos, int zoom) const override;

  /**
   * 获取高德地图的id
   * @return 高德地图id
   */
  const QString &id() const override { return mId; }

 private:
  const QString mId{"gaodenormalmap"};
};

}