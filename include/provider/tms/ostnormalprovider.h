#pragma once

#include <QtCore/QSize>

#include <pointxy.h>
#include <provider/tmsprovider.h>
#include <utils/easygis.h>

namespace EasyGIS {

/**
 * OpenStreetMap基础地图
 */
class OSTNormalProvider : public TmsProvider {
  Q_OBJECT

 public:
  explicit OSTNormalProvider(QObject *parent= nullptr);
  ~OSTNormalProvider() override = default;

 public:
  /**
   * 获取瓦片的大小
   * @return 瓦片大小，一般为256*256
   */
  const QSize tileSize() const override {
    return QSize{256, 256};
  }

  /**
   * 获取openstreetmap的瓦片服务器编号
   * @return
   */
  char server() const { return static_cast<char>('a' + randomInt(0, 3)); }

  /**
   * 获取瓦片url路径
   * @param pos 瓦片位置
   * @param zoom zoom值
   * @return 瓦片url，失败时为空字符串
   */
  QString tileUrl(const PointXY &pos, int zoom) const override;

  /**
   * 获取opensteet地图id
   * @return opensteet地图id
   */
  const QString& id() const override{return mId;}

 private:
  const QString mId{"ostnormalmap"};
};

}