#pragma once

#include <QtGui/QImage>
#include <QtCore/QMap>
#include <QtCore/QPointF>

namespace EasyGIS {
/**
 * 用于图层的渲染数据提供，每个图层必须包含与其对应的provider。
 */
class LayerProvider : public QObject {
 Q_OBJECT

 public slots:
  virtual void createTask(const QRectF &rect, int zoom) = 0;

 public:
  explicit LayerProvider(QObject *parent = nullptr)
      : QObject(parent) {}
  ~LayerProvider() override = default;

 public:
  /**
   * 返回可显示的图像内容
   * @return 图像对象
   */
  virtual const QImage preparedImage() const = 0;

  /**
   * 判断是否有数据可以使用
   * @return 有数据时为true，否则fasle
   */
  virtual bool hasContent() const = 0;
};

}
