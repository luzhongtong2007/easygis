#pragma once

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QVector>
#include <QLabel>
#include <QLineEdit>
#include <QWidget>
#include <QActionGroup>
#include <QListWidgetItem>
#include <QHash>

#include <mapcanvas.h>
#include <maplayer.h>

namespace Ui {
class MainWindow;
}

namespace EasyGIS {

class MainWindow : public QMainWindow {
 Q_OBJECT

 friend class TaskWindow;
 friend class DownloadTask;

 public slots:
  void changeTaskTable(int row, int col, QString text);

 public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow() override;

 protected:
  void setupWindow();
  void setupActions();
  void setupStatusBar();
  void setupLayers();
  void setupTaskWindow();
  void initMaps();

 protected:
  /// 各处处理函数
  void panHandle(bool checked);
  void zoomInHandle(bool checked);
  void zoomOutHandle(bool checked);
   void drawlineHandle(bool checked);
  void tutorialHanle(bool checked);
  void srcHandle(bool checked);
  void sponsorHandle(bool checked);
  void refreshHandle(bool checked);
  void selectHandle(bool checked);
  void zoomChangedHandle(int zoom);
  void clickedHandle(PointXY pos);
  void centerChangedHandle(PointXY pos);
  void leftTopClickedHandle();
  void rightBottomClickedHandle();
  void createDownloadTask();
  void layerChanged(QListWidgetItem *current, QListWidgetItem *previous);



 private:
  QWidget *spacerWiget(int width) const;

 protected:
  static QString tutorialUrl();
  static QString srcUrl();

private slots:
  void on_drawArea_triggered();

  void on_addPlaneaction_triggered();

private:
  Ui::MainWindow *mUi;
  MapCanvas *mMapConvas;
  QLineEdit *mScaleText;
  QLabel *mScaleLabel;
  QLineEdit *mCenterText;
  QLabel *mCenterLabel;
  QLineEdit *mZoomText;
  QLabel *mZoomLabel;
  QActionGroup *mMapActionGroup;
  bool mSetLeftTop;
  QVector<QListWidgetItem*> mLayerList;
  PointXY mLeftTop;
  PointXY mRightBottom;

 private:
  static QHash<QString, MapLayer*> mMaps;
};

}
