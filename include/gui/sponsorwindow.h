#pragma once

#include "ui_sponsorwindow.h"
#include <QtWidgets/QDialog>

namespace Ui {
class SponsorWindow;
}

class SponsorWindow : public QDialog {
 Q_OBJECT

 public:
  explicit SponsorWindow(QWidget *parent = 0);
  ~SponsorWindow();

 protected:
  void UiSetup();
  void Initialize();

  void WxPayClickedHandle();
  void AliPayClickedHandle();

 private:
  Ui::SponsorWindow *ui;
};
