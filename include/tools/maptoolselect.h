#pragma once

#include <QtCore/QRect>
#include <QtGui/QMouseEvent>
#include <QtCore/QString>

#include <mapcanvas.h>
#include <maptool.h>

namespace EasyGIS {
/**
 * 地图拖动工具
 */
class MapToolSelect : public MapTool {
 public:
  explicit MapToolSelect(MapCanvas *mapCanvas):MapTool(mapCanvas){}
  ~MapToolSelect() override = default;

 public:
  void execute(QMouseEvent *event) override;
  void setup() override;
  void deSetup() override;
  QString id() override;

 private:
  QPoint mDragStartPos;
  QPoint mDragEndPos;
};

}


