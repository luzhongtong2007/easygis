#pragma once

#include <QtCore/QRect>
#include <QtGui/QMouseEvent>
#include <QtCore/QString>

#include <mapcanvas.h>
#include <maptool.h>

namespace EasyGIS {
/**
 * 地图拖动工具
 */
class MapToolPan : public MapTool {
 public:
  explicit MapToolPan(MapCanvas *mapCanvas):MapTool(mapCanvas){}
  ~MapToolPan() override = default;

 public:
  void execute(QMouseEvent *event) override;
  void setup() override;
  void deSetup() override;
  QString id() override;

 private:
  QPoint mDragStartPos;
  QPoint mDragEndPos;
};

}

