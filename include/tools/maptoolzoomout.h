#pragma once

#include <QtCore/QRect>
#include <QtGui/QMouseEvent>
#include <QtCore/QString>

#include <mapcanvas.h>
#include <maptool.h>

namespace EasyGIS {
/**
 * 地图缩放工具
 */
class MapToolZoomOut : public MapTool {
 public:
  MapToolZoomOut(MapCanvas *mapCanvas):MapTool(mapCanvas){}
  virtual ~MapToolZoomOut() = default;

 public:
  void execute(QMouseEvent *event) override;
  void setup() override;
  void deSetup() override;
  QString id() override;
};

}



