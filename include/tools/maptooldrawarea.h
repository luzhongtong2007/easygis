#ifndef MAPTOOLDRAWAREA_H
#define MAPTOOLDRAWAREA_H
#include <maptool.h>
namespace  EasyGIS{
class MapCanvas;
class Maparea;
class MapToolDrawarea:public MapTool
{
public:
 explicit MapToolDrawarea(MapCanvas *mapCanvas):MapTool(mapCanvas),isDrawing(false),current_area(nullptr){}
 ~MapToolDrawarea() override = default;

public:
    void execute(QMouseEvent *event) override;
    void setup() override;
    void deSetup() override;
    QString id() override;
private:
    bool isDrawing;
    Maparea * current_area;
};
}
#endif // MAPTOOLDRAWAREA_H
