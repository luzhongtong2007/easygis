#ifndef MAPTOOLDRAWLINE_H
#define MAPTOOLDRAWLINE_H


#include <maptool.h>

namespace EasyGIS {
class Mapline;
class MapCanvas;
/**
 * @brief 绘制航线工具
 */
class MapToolDrawline: public MapTool
{
public:
 explicit MapToolDrawline(MapCanvas *mapCanvas):MapTool(mapCanvas),isDrawing(false),current_line(nullptr){}
 ~MapToolDrawline() override = default;

public:
    void execute(QMouseEvent *event) override;
    void setup() override;
    void deSetup() override;
    QString id() override;
private:
    bool isDrawing;
    Mapline* current_line;

};
}

#endif // MAPTOOLDRAWLINE_H
