#pragma once

#include <QString>

namespace EasyGIS
{
  class ImgWriter
  {
  public:
    ImgWriter(QString file, const QSize& size);
    ImgWriter(const ImgWriter& other) = delete;
    ImgWriter(ImgWriter&& other) = delete;
    ImgWriter operator =(const ImgWriter& other) = delete;
    ImgWriter operator =(ImgWriter&& other) = delete;

    virtual ~ImgWriter() = default;

  public:
    bool write(const QPoint& pos, const QByteArray& data) const;

  protected:
    QString mFile;
  };
}
