#pragma once

#include <QtCore/QString>
#include <QtCore/QByteArray>
#include <curl/curl.h>

namespace EasyGIS {
/**
 * http或https请求使用的返回数据接收函数
 * @param content 返回数据
 * @param size 大小
 * @param nmemb 内存字节数
 * @param userp 数据存储位置
 * @return 成功写入多少字节
 */
size_t
writeData(void *content, size_t size, size_t nmemb, void *userp);

class Network {
 public:
  Network() { curl_global_init(CURL_GLOBAL_DEFAULT); }
  ~Network() { curl_global_cleanup(); }

 public:
  /**
   * http的Get请求
   * @param url 需要访问的地址
   * @return 返回的数据
   */
  QByteArray
  httpRequest(const QString &url);

  /**
   * https的Get请求
   * @param url 请求的地址
   * @return 返回的数据
   */
  QByteArray
  httpsRequest(const QString &url);

 private:
  static const QString kUserAgent;
};

}