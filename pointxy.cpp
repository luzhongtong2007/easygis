#include <QtCore/QtMath>
#include <QtCore/QString>
#include <QtCore/QDebug>

#include <pointxy.h>
#include <utils/easygis.h>

namespace EasyGIS {

double
PointXY::distance(double x, double y) const {
  return qSqrt(qPow(mX - x, 2) + qPow(mY - y, 2));
}

bool
PointXY::operator==(const EasyGIS::PointXY &other) {
  return isDoubleNearby(mX, other.mX) && isDoubleNearby(mY, other.mY);
}

PointXY &
PointXY::operator=(const EasyGIS::PointXY &other) {
  if (this != &other) {
    mX = other.mX;
    mY = other.mY;
  }

  return *this;
}

PointXY &
PointXY::operator=(const QPointF &other) {
  mX = other.x();
  mY = other.y();

  return *this;
}

PointXY &
PointXY::operator=(const QPoint &other) {
  mX = double(other.x());
  mY = double(other.y());

  return *this;
}

QString
PointXY::toWKT() const {
  return QString("POINT(%1 %2)").arg(mX, 0, 'g', 8).arg(mY, 0, 'g', 8);
}

}

QDebug operator<<(QDebug debug, const EasyGIS::PointXY &point) {
  debug << QString("PointXY(%1, %2)").arg(point.x()).arg(point.y());
  return debug;
}
