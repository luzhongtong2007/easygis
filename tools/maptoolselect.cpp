#include <QString>

#include <tools/maptoolselect.h>

namespace EasyGIS
{

void
MapToolSelect::execute(QMouseEvent *event) {
  if(!(event->button() & Qt::LeftButton)){
    return;
  }

  if(event->type() == QEvent::MouseButtonPress){
    auto pos = mMapCanvas->pixel2Lonlat(event->pos());
    mMapCanvas->clicked(pos);
  }
}

void
MapToolSelect::setup() {
  mMapCanvas->setCursor(Qt::PointingHandCursor);
}

void
MapToolSelect::deSetup() {
  mMapCanvas->setCursor(Qt::ArrowCursor);
}

QString
MapToolSelect::id() {
  return QString{"select_tool"};
}

}

