#include "tools/maptooladdplane.h"
#include "mapcanvas.h"
#include "maplayer.h"
#include "mapautoplane.h"
void EasyGIS::MapToolAddplane::execute(QMouseEvent *event)
{
    QPointF scene_point=this->mMapCanvas->mapToScene(event->pos());
    QPointF mkt_point=scene_point*this->mMapCanvas->currentLayer()->resolution();
    if( event->type() != QEvent::MouseButtonPress) return;
    if(event->button()==Qt::LeftButton){
        MapAutoplane*plane=new MapAutoplane(this->mMapCanvas->currentLayer(),mkt_point);
        this->addPlane(plane);
    }
}

void EasyGIS::MapToolAddplane::setup()
{

}

void EasyGIS::MapToolAddplane::deSetup()
{

}

QString EasyGIS::MapToolAddplane::id()
{
    return QString("addplane_tool");
}

void EasyGIS::MapToolAddplane::addPlane(MapAutoplane *plane)
{
    this->mMapCanvas->scene()->addItem(plane);
    this->planes.append(plane);
}

void EasyGIS::MapToolAddplane::delePlane(MapAutoplane *plane)
{
    this->mMapCanvas->scene()->removeItem(plane);
    this->planes.removeAll(plane);
}
