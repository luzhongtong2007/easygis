#include "tools/maptooldrawarea.h"
#include "mapcanvas.h"
#include "maparea.h"
#include "maplayer.h"
void EasyGIS::MapToolDrawarea::execute(QMouseEvent *event)
{
    QPointF scene_point=this->mMapCanvas->mapToScene(event->pos());
    QPointF mkt_point=scene_point*this->mMapCanvas->currentLayer()->resolution();
    if(event->type()==QEvent::MouseMove&&isDrawing){
        this->current_area->lastMktpoint()=mkt_point;
        qDebug()<<"mouse move";
    }

    if( event->type() != QEvent::MouseButtonPress) return;
    if(event->button()==Qt::LeftButton){
        if(!isDrawing){
            qDebug()<<"begin drawarea";
            isDrawing=true;
            this->current_area=new Maparea(this->mMapCanvas->currentLayer());
            this->mMapCanvas->scene()->addItem(current_area);
            this->current_area->append(mkt_point);
        }
        this->current_area->append(mkt_point);
    }
    if(event->button()==Qt::RightButton){
        isDrawing=false;
    }
}

void EasyGIS::MapToolDrawarea::setup()
{
    mMapCanvas->setCursor(Qt::CrossCursor);
}

void EasyGIS::MapToolDrawarea::deSetup()
{
    mMapCanvas->setCursor(Qt::ArrowCursor);
}

QString EasyGIS::MapToolDrawarea::id()
{
    return QString("drawarea_tool");
}
