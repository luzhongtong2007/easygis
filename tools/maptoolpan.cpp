#include <QtCore/QEvent>

#include <tools/maptoolpan.h>
#include <mapcanvas.h>

namespace EasyGIS {

void
MapToolPan::execute(QMouseEvent *event) {
  if(!(event->button() & Qt::LeftButton)){
    return;
  }

  auto type = event->type();
  if (QEvent::MouseButtonPress == type) {
    mDragStartPos = event->pos();
  }

  if (QEvent::MouseButtonRelease == type) {
    mDragEndPos = event->pos();
    QRectF dragRect{mDragStartPos, mDragEndPos};
    mMapCanvas->mDragRect = dragRect;
    mMapCanvas->updateViewExtent();
  }
}

void
MapToolPan::setup() {
  mMapCanvas->setDragMode(MapCanvas::DragMode::ScrollHandDrag);
}

QString
MapToolPan::id() {
  return QString{"pan_tool"};
}

void
MapToolPan::deSetup() {
  mMapCanvas->setDragMode(MapCanvas::DragMode::NoDrag);
}

}

