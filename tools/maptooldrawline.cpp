#include "tools/maptooldrawline.h"
#include <QDebug>

#include "mapline.h"
#include "mapcanvas.h"
#include "maplayer.h"

void EasyGIS::MapToolDrawline::execute(QMouseEvent *event)
{
    QPointF scene_point=this->mMapCanvas->mapToScene(event->pos());
    QPointF mkt_point=scene_point*this->mMapCanvas->currentLayer()->resolution();
    if(event->type()==QEvent::MouseMove&&isDrawing){
        this->current_line->lastMktpoint()=mkt_point;
    }

    if( event->type() != QEvent::MouseButtonPress) return;
    if(event->button()==Qt::LeftButton){
        if(!isDrawing){
            qDebug()<<"begin drawline";
            isDrawing=true;
            this->current_line=new Mapline(this->mMapCanvas->currentLayer());
            this->mMapCanvas->scene()->addItem(current_line);
            this->current_line->append(mkt_point);
        }
        this->current_line->append(mkt_point);
    }
    if(event->button()==Qt::RightButton){
        isDrawing=false;
        qDebug()<<"end drawline";
    }
}

void EasyGIS::MapToolDrawline::setup()
{
    mMapCanvas->setCursor(Qt::CrossCursor);
}

void EasyGIS::MapToolDrawline::deSetup()
{
    mMapCanvas->setCursor(Qt::ArrowCursor);
}

QString EasyGIS::MapToolDrawline::id()
{
    return QString("drawline_tool");
}
