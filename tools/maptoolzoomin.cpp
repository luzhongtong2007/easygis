#include <QtCore/QString>
#include <QtCore/QEvent>

#include <tools/maptoolzoomin.h>
#include <include/tools/maptoolzoomin.h>

namespace EasyGIS {

void
MapToolZoomIn::execute(QMouseEvent *event) {
  if(!(event->button() & Qt::LeftButton) || event->type() != QEvent::MouseButtonPress){
    return;
  }

  int zoom = mMapCanvas->zoomValue();
  ++zoom;
  mMapCanvas->setZoomValue(zoom);
}

void
MapToolZoomIn::setup() {
  /// do nothing
}

QString
MapToolZoomIn::id() {
  return QString{"zoomin_tool"};
}

void
MapToolZoomIn::deSetup() {
  /// do nothing
}

}

