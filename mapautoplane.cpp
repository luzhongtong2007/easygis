#include "mapautoplane.h"
#include "maplayer.h"
#include <QRandomGenerator>
#include <QDebug>
EasyGIS::MapAutoplane::MapAutoplane(const MapLayer *layer,QPointF pos,QGraphicsItem *parent):mLayer(layer), current_mktpoint(pos),QGraphicsPixmapItem(parent)
{
    QPixmap pix(":/plane-red.png");
    this->setPixmap(pix);
    QPointF scene_point=this->current_mktpoint/this->mLayer->resolution();
    this->setOffset(scene_point);
}

void EasyGIS::MapAutoplane::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    QPointF scene_point=this->current_mktpoint/this->mLayer->resolution();
    this->setOffset(scene_point);
    QGraphicsPixmapItem::paint(painter,option,widget);
}

QRectF EasyGIS::MapAutoplane::boundingRect() const
{
     QPointF scene_point=this->current_mktpoint/this->mLayer->resolution();
     return QRectF(scene_point.x(),scene_point.y(),16,16);
}

void EasyGIS::MapAutoplane::updatePos()
{
     qreal x=QRandomGenerator::global()->bounded(-1000, 1000);
     qreal y=QRandomGenerator::global()->bounded(-1000, 1000);
     this->current_mktpoint+=QPointF(x,y);
}


